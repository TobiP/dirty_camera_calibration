#!/usr/bin/env python
import numpy as np
import os

NEWLINE_SIZE_IN_BYTES = -1

def appendMatrixToFile(filename, matrix, matrixName):
    with open(filename, "a") as f:
        f.write(matrixName + ':\n')
        f.write('  rows: ' + str(matrix.shape[0]) + '\n')
        f.write('  cols: ' + str(matrix.shape[1]) + '\n')
        f.write('  data: [')
        np.savetxt(f, matrix, delimiter=', ', fmt='%.6f', newline=',\n         ')
        
        # Remove last delimiter
        f.seek(0, os.SEEK_END)
        lastIndex = f.tell() -11 # -11 because of indentations
        print(lastIndex)
        f.seek(lastIndex, os.SEEK_SET)
        f.truncate()

        f.write(']\n')