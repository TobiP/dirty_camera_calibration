#!/usr/bin/env python
import file_output as output
import numpy as np
import os


matrix = np.array([[123423.345343534, 4.23423222, 4],
                   [3, 4, 53453.34534],
                   [3, 4, 2345.232342342]])
filename = "output/test.yaml"
os.remove(filename)
output.appendMatrixToFile(filename, matrix, "camera_matrix")
